import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by amen on 8/7/17.
 */
public class Magazine {
    private Map<ProductClass, List<Product>> map = new HashMap<>();
//    private List<Product> list = new LinkedList<>();

    public void addProduct(String name, Double value, ProductClass pclass, ProductType type) {
        Product p = new Product(name, value, type, pclass);

        // dodwaanie któregoś z kolei elementu do listy danej klasy
        if (map.containsKey(pclass)) {
            List<Product> listOfClass = map.get(pclass);

            listOfClass.add(p);

            map.put(pclass, listOfClass);
        } else {// dodajemy pierwszy element
            List<Product> newProductList = new LinkedList<>();
            newProductList.add(p);

            map.put(pclass, newProductList);
        }
    }

    public List<Product> getByClass(ProductClass pclass) {
        return map.get(pclass);
    }

    public List<Product> getByType(ProductType type) {
        List<Product> filteredProducts = new LinkedList<>();
        for (List<Product> productClassList : map.values()) {
            for (Product singleProduct : productClassList) {
                if (singleProduct.getType() == type) {
                    filteredProducts.add(singleProduct);
                }
            }
        }
        return filteredProducts;
    }


    public void saveToExcel() throws IOException{
        String fileName = "out.xls";
        Workbook workbook = null;

        if(fileName.endsWith("xlsx")){
            workbook = new XSSFWorkbook();
        }else if(fileName.endsWith("xls")){
            workbook = new HSSFWorkbook();
        }

        Sheet sheet = workbook.createSheet("Products");
        int rowNumber = 0;
        for (List<Product> productClassList : map.values()) {
            for (Product singleProduct : productClassList) {
                Row r = sheet.createRow(rowNumber);
                Cell c1 = r.createCell(0);
                c1.setCellValue(singleProduct.getName());
                Cell c2 = r.createCell(1);
                c2.setCellValue(singleProduct.getCena());
                Cell c3 = r.createCell(2);
                c3.setCellValue(singleProduct.getType().toString());
                Cell c4 = r.createCell(3);
                c4.setCellValue(singleProduct.getpClass().toString());

            }
        }
        FileOutputStream fos = new FileOutputStream(fileName);
        workbook.write(fos);
        fos.close();
    }
}
