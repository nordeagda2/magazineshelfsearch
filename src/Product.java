/**
 * Created by amen on 8/7/17.
 */
public class Product {
    private String name;
    private double cena;
    private ProductType type;
    private ProductClass pClass;

    public Product() {
    }

    public Product(String name, double cena, ProductType type, ProductClass pClass) {
        this.name = name;
        this.cena = cena;
        this.type = type;
        this.pClass = pClass;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return name + "::" + cena + "::" + type + "::" + pClass;
    }

    public ProductClass getpClass() {
        return pClass;
    }
}
