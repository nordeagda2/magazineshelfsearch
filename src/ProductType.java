/**
 * Created by amen on 8/7/17.
 */
public enum ProductType {
    FOOD, INDUSTRIAL, ALCOHOL
}
